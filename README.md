# qonto-test

Cette application est développé sur la base du design pattern MVP, en séparant au mieux les vues (activité/fragments), les presenter, et la couche data.  
Chaque vue implémente une interface (un contrat), qui va servir à limité la dépendance entre les différentes couche logiciel tout en leur permettant de communiquer les une avec les autres.  
Cette interface est envoyé à une classe Presenter qui va s'occuper de faire l'intermédiaire entre la vue les datas. Cette classe utilise notamment RxAndroid pour une gestion poussé des événements.  
Le Presenter posséde également une interface lui permettant de communiquez avec la couche réseau de l'application afin de récupérer les données depuis un End point donné.  

Ce qui à été fait :

Configuration des dépendances  
Structure de l'application (MVP)  
Couche réseau  
Récupération/filtrage des données depuis l'api  

Ce qu'il reste a faire :

Affichage des données sous forme de liste  
Implémentation de la pagination pour la récupération des données   
Persistance des données en local a l'aide de Realm  
Création d'un fragment pour l'affichage des informations complétes sur un user selectionné  
Lissage graphique (animation / design)


