package com.quonto.randomuser.common;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class EndlessRecyclerScroll extends RecyclerView.OnScrollListener {

    private final static int VISIBLE_THRESHOLD = 2;

    private final LinearLayoutManager layoutManager;
    private final OnEndlessScrollListener listener;

    private int currentPage = 1;

    public EndlessRecyclerScroll(LinearLayoutManager layoutManager, OnEndlessScrollListener listener) {
        this.layoutManager = layoutManager;
        this.listener = listener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        final OnEndlessAdapterListener adapterListener = ((OnEndlessAdapterListener) recyclerView.getAdapter());
        final int visibleItemCount = layoutManager.getChildCount();
        final int totalItemCount = layoutManager.getItemCount();
        final int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
        if (dy > 0) {
            if (!adapterListener.isLoading() && (visibleItemCount + firstVisibleItem + VISIBLE_THRESHOLD) >= totalItemCount) {
                adapterListener.setLoading(true);
                listener.onLoadMore(++currentPage);
            }
        }
    }

    public interface OnEndlessScrollListener {
        void onLoadMore(int currentPage);
    }

    public interface OnEndlessAdapterListener {
        boolean isLoading();
        void setLoading(boolean isLoading);
    }

    public void decrementCurrentPage() {
        currentPage--;
    }

}
