package com.quonto.randomuser.common;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.quonto.randomuser.BuildConfig;
import com.quonto.randomuser.user.api.UserApi;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class NetworkUtils {

    public static UserApi buildUserApi() {
        return buildSSLApi(Config.BASE_URL, UserApi.class);
    }


    private static <T> T buildSSLApi(String url, Class<T> apiClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(BuildConfig.DEBUG ? UnsafeOkHttpClient.createClient() : getSSLOkHttpClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        //if (Config.IS_TEST) {
        //}
        return retrofit.create(apiClass);
    }

    public static OkHttpClient getSSLOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.addInterceptor(new LoggingInterceptor());

        return builder.build();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isWifiAvailable(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    // Allow to print information on the requests
    public static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {

            Request request = chain.request();
            long t1 = System.nanoTime();
            Request result = request.newBuilder()
                    .header("App-Version", "1.0.0")
                    .method(request.method(), request.body())
                    .build();

            Log.d("Retrofit", String.format("Sending request %s on %s%n%s", request.url(), request.isHttps() ? "HTTPS" : "HTTP", request.headers()));
            if (request.body() != null) {
                Log.d("Retrofit", String.format("Body : %s", bodyToString(request)));
            }

            Response response = chain.proceed(result);
            long t2 = System.nanoTime();

            Log.d("Retrofit", String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            final String responseString = new String(response.body().bytes());

            Log.d("Retrofit", "Response: " + responseString);

            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), responseString))
                    .build();
        }
    }

    private static String bodyToString(final Request request){

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
