package com.quonto.randomuser.common;

import android.content.Intent;
import android.os.Bundle;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.util.List;

public class IntentHelper {

    public static ObjectMapper objectMapper;
    private static TypeFactory typeFactory = TypeFactory.defaultInstance();

    static {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static void put(Intent intent, String key, Object object) {
        try {
            intent.putExtra(key, objectMapper.writeValueAsString(object));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void put(Bundle bundle, String key, Object object) {
        try {
            bundle.putString(key, objectMapper.writeValueAsString(object));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static <T> T get(Intent intent, String key, Class<T> classToLoad) {
        return get(intent.getExtras(), key, classToLoad);
    }

    public static <T> T get(Bundle bundle, String key, Class<T> classToLoad) {
        try {
            return objectMapper.readValue(bundle.getString(key), classToLoad);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> List<T> getList(Intent intent, String key, Class<T> classToLoad) {
        return getList(intent.getExtras(), key, classToLoad);
    }

    public static <T> List<T> getList(Bundle bundle, String key, Class<T> classToLoad) {
        try {
            if (bundle.getString(key) != null) {
                return objectMapper.readValue(bundle.getString(key), typeFactory.constructCollectionType(List.class, classToLoad));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
