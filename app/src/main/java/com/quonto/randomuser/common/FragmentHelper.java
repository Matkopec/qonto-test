package com.quonto.randomuser.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

public class FragmentHelper {

    public static void replaceFragment(Fragment containerFragment, int containerId, Fragment fragmentToAdd, String tag) {
        containerFragment.getChildFragmentManager()
                .beginTransaction()
                .replace(containerId, fragmentToAdd, tag)
                .commitAllowingStateLoss();
        containerFragment.getChildFragmentManager().executePendingTransactions();
    }

    public static void replaceFragment(FragmentActivity containerActivity, int containerId, Fragment fragmentToAdd, String tag) {
        replaceFragment(containerActivity, containerId, fragmentToAdd, tag, false);
    }

    public static void replaceFragment(FragmentActivity containerActivity, int containerId, Fragment fragmentToAdd, String tag, boolean addToBackStack) {
        FragmentTransaction transac = containerActivity.getSupportFragmentManager().beginTransaction();
        transac.replace(containerId, fragmentToAdd, tag);
        if (addToBackStack) {
            transac.addToBackStack(tag);
        }
        transac.commitAllowingStateLoss();
        containerActivity.getSupportFragmentManager().executePendingTransactions();
    }
}
