package com.quonto.randomuser.common.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "results"
})
public class ApiResponse<T> {

    @JsonProperty("results")
    private T data;

    /**
     * @return The data
     */
    @JsonProperty("results")
    public T getData() {
        return data;
    }

    /**
     * @param data The data
     */
    @JsonProperty("results")
    public void setData(T data) {
        this.data = data;
    }

}
