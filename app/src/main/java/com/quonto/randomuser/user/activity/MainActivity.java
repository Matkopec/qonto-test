package com.quonto.randomuser.user.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.quonto.randomuser.R;
import com.quonto.randomuser.common.FragmentHelper;
import com.quonto.randomuser.user.fragment.UserListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    final static String TAG = "MainActivity";

    @BindView(R.id.main_layout_frame)
    public FrameLayout mFragmentContainer;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);
        initFragments();
    }

    private void initFragments() {
        mFragmentContainer.removeAllViewsInLayout();
        UserListFragment fragment = UserListFragment.newInstance();

        FragmentHelper.replaceFragment(this, R.id.main_layout_frame, fragment, TAG);
    }
}
