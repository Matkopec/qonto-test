package com.quonto.randomuser.user.presenter;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class Presenter {

    protected CompositeSubscription compositeSubscription = new CompositeSubscription();

    public void link(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    public void unlink() {
        compositeSubscription.clear();
    }

    public void start() {
    }

    public void resume() {

    }

    public void pause() {

    }

    public void stop() {

    }

    public void destroy() {
        unlink();
    }

}
