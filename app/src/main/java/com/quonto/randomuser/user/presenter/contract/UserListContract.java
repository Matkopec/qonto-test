package com.quonto.randomuser.user.presenter.contract;

import com.quonto.randomuser.user.model.User;

import java.util.List;

public interface UserListContract {

    public void displayUsers(List<User> users);

    public void showLoading(boolean show);

}
