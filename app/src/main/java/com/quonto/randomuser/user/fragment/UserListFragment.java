package com.quonto.randomuser.user.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.quonto.randomuser.R;
import com.quonto.randomuser.user.presenter.UserPresenter;
import com.quonto.randomuser.user.presenter.contract.UserListContract;
import com.quonto.randomuser.user.model.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserListFragment extends PresenterFragment<UserPresenter> implements UserListContract {

    @BindView(R.id.user_list_fragment_progress)
    public ProgressBar progressBar;

    public final String TAG = "UserListFragment";

    public UserListFragment() {
        // Required empty public constructor
    }

    public static UserListFragment newInstance() {
        return new UserListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_list, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setPresenter(new UserPresenter(this));
        getPresenter().getUserList(1);
    }

    @Override
    public void displayUsers(List<User> users) {
        Log.d(TAG, "users recieved : " + users.size());
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

}
