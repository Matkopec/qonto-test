package com.quonto.randomuser.user.api;

import com.quonto.randomuser.common.Model.ApiResponse;
import com.quonto.randomuser.user.model.User;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface UserApi {

    @GET("/api/")
    Observable<ApiResponse<List<User>>> getUsers(@Query("results") int resultsNumber, @Query("page") int pageNumber);

}
