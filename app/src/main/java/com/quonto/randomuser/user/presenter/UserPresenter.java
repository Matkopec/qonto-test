package com.quonto.randomuser.user.presenter;

import android.content.Context;

import com.quonto.randomuser.common.Constants;
import com.quonto.randomuser.common.Model.ApiResponse;
import com.quonto.randomuser.common.NetworkUtils;
import com.quonto.randomuser.common.RxComposer;
import com.quonto.randomuser.user.api.UserApi;
import com.quonto.randomuser.user.model.User;
import com.quonto.randomuser.user.presenter.contract.UserListContract;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class UserPresenter extends Presenter {

    private UserListContract viewContract;
    private UserApi userApi;

    public UserPresenter(UserListContract itemContract) {
        super();
        viewContract = itemContract;
        userApi = NetworkUtils.buildUserApi();
    }

    public void getUserList(int page) {
        viewContract.showLoading(true);
        Observable<ApiResponse<List<User>>> observable$ = userApi.getUsers(Constants.USER_RESULT_NUMBER, page);

        link(observable$.compose(RxComposer.io())
                .filter(response -> response.getData() != null)
                .subscribe(response -> {
                    viewContract.showLoading(false);
                    //viewContract.showError(false);
                    viewContract.displayUsers(response.getData());
                }, throwable -> {
                    throwable.printStackTrace();
                    viewContract.showLoading(false);
                    //viewContract.showError(true);
                    viewContract.displayUsers(new ArrayList<User>());
                }));
    }
}
